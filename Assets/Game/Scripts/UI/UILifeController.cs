﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class UILifeController : MonoBehaviour
{
    //atributo para que se pueda ver desde el editor de unity
    [SerializeField] Slider slider; //componente de ui que vamos a actualizar 
    [SerializeField] LifeController playerLife;

    [SerializeField] Image fillImage; //referencia a el color de la barra de vida
    [SerializeField] Gradient lifeColor;
    private void Update()
    {
        float life = ((float) playerLife.GetCurrentLife()) / ((float)playerLife.GetCurrentMaxLife()); //10/100
        slider.value = life;//vida [0-1]
        fillImage.color = lifeColor.Evaluate(life);

            /* NORMALIZAR VALORES = Convertir a [0-1]
             current life | maxlife | c/ml
            10            | 100     | 10/100 = 0.1
            20            | 100     | 20/200 = 0.2
            50            | 100     | 50/200 = 0.5
             
             */
    }
}
