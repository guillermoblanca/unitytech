﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum EAffinity
{
    Neutral ,
    Ally,
    Enemy
}

[Flags]
public enum EWeaponTypes
{
    Melee = 1, 
    Distance =2 , 
    Magic = 4,
    Hands = 8
}