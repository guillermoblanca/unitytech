﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILifeDispatcher : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] Image fillImage;
    [SerializeField] LifeController playerLife;
    [SerializeField] Gradient gradientLife;
    float currentValue;
    /// <summary>
    /// BAD
    /// </summary>
    private void Update()
    {
        if(playerLife)
        {
            UpdateSlider(0);
        }
    }



    public void UpdateSlider(int newLife)
    {
        float newValue = ((float)playerLife.GetCurrentLife()) / ((float)playerLife.GetCurrentMaxLife());

        slider.value = newValue;

        Color colorGradient = gradientLife.Evaluate(newValue);
        fillImage.color = colorGradient;
    }
}
