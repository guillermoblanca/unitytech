﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEditorInternal;
using UnityEngine;

public class FireWeapon : MonoBehaviour
{
    [Header("References")]
    public GameObject[] firePoints;
    public GameObject particlePrefab;
    [Header("Audio")]
    public AudioClip fireAudio;
    public AudioClip reloadAudio;

    [Header("Cadence")]
    public float fireRate = 0.4f;
    public float distance = 100;
    [Header("Bullets")]
    public int maxBulletsInWeapon = 10;
    public int bulletsPerShoot = 1;
    [SerializeField] int currentBullets = 0;

    [Header("Damage")]
    public int damage = 10;

    private int bulletsInWeapon = 0;
    private bool isShooting = false;
    private float timer = 0.0f;
    private Camera cameraRef;

    private const string m_FireInput = "Fire1";
    public void Start()
    {
        bulletsInWeapon = maxBulletsInWeapon;
        cameraRef = Camera.main;
    }


    public void Update()
    {
        timer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }

        if (Input.GetButton(m_FireInput))
        {
            TryToShoot();
        }
        else if (Input.GetButtonUp(m_FireInput))
        {
            StopShoot();
        }
    }

    public void TryToShoot()
    {
        if (isShooting)
        {
            return;
        }

        if (CanShoot())
        {


            StartShoot();
        }
    }

    public bool CanShoot()
    {
        if (timer >= fireRate && bulletsInWeapon > 0)
        {
            return true;
        }
        return false;
    }
    private void StartShoot()
    {
        isShooting = true;
        bool hasBullets = ConsumeBullets();

        if (hasBullets)
        {
            Shoot();
        }
        else
        {
            StopShoot();
            Reload();

        }

    }

    private void Shoot()
    {
        Vector3 origin = cameraRef.transform.position;
        Vector3 direction = cameraRef.transform.forward;

        if (Physics.Raycast(origin, direction, out RaycastHit hit, distance))
        {
            GameObject objectHitted = hit.collider.gameObject;

            //this should be damageable component for creating different points for shooting
            LifeController life = objectHitted.GetComponent<LifeController>();
            if (life)
            {
                life.ApplyDamage(damage);
            }
        }

        isShooting = false;
        timer = 0.0f;

    }
    private void StopShoot()
    {
        isShooting = false;
    }
    public void Reload()
    {
        if (isShooting)
        {
            StopShoot();
        }

        int bulletsToAdd = (maxBulletsInWeapon - bulletsInWeapon);
        Debug.Log("bullets to add " + bulletsToAdd);

        if( currentBullets - bulletsToAdd >= 0)
        {
            bulletsInWeapon += bulletsToAdd;
            currentBullets -= bulletsToAdd;
        }
        else
        {
            bulletsInWeapon += currentBullets;
            currentBullets = 0;
        }

        if (reloadAudio)
        {
            AudioSource source = GetComponent<AudioSource>();
            if (source)
            {
                source.PlayOneShot(reloadAudio);
            }
        }
        Debug.Log("Reloading");
    }


    public void AddBullets(int bullets)
    {
        currentBullets += bullets;

        if (currentBullets > maxBulletsInWeapon)
        {
            currentBullets = maxBulletsInWeapon;
        }

        if (bulletsInWeapon < maxBulletsInWeapon)
        {
            Reload();
        }
    }

    private bool ConsumeBullets()
    {
        if (bulletsInWeapon <= 0)
        {
            bulletsInWeapon = 0;
            return false;
        }

        int newBullets = bulletsInWeapon - bulletsPerShoot;

        if (newBullets < 0)
        {
            return false;

        }
        else
        {
            bulletsInWeapon = newBullets;
        }
        return true;
    }

    private void OnGUI()
    {
        Rect r = new Rect(0, 0, 150, 40);

        string[] info =
        {
            "Can shoot" + CanShoot(),
            "Bullets in weapon " + bulletsInWeapon,
            "Bullets "  + currentBullets,

        };


        for (int i = 0; i < info.Length; i++)
        {
            GUI.Box(r, info[i]);
            r.y += r.height;
        }
    }
}