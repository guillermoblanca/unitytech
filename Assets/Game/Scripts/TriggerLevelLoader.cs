﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerLevelLoader : MonoBehaviour
{
    public string[] Levels;

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < Levels.Length; i++)
        {
            SceneManager.LoadScene(Levels[i], LoadSceneMode.Additive);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        for (int i = 0; i < Levels.Length; i++)
        {
            SceneManager.UnloadSceneAsync(Levels[i]);
        }
    }
}
