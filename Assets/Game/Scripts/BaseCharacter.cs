﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BaseCharacter : MonoBehaviour
{
    public EAffinity affinity;
    public EWeaponTypes weapons;

    private void Start()
    {
        switch (affinity)
        {
            case EAffinity.Neutral:
                Debug.Log("Soy neutral");
                break;
            case EAffinity.Ally:
                Debug.Log("Soy aliado");
                break;
            case EAffinity.Enemy:
                Debug.Log("Soy enemigo");
                break;
            default:
                break;
        }
    }
}