﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    GameObject target;
    public LifeController life;
    public Animator animator;

    public float attackdistance = 0.5f;
    public float attackrate = 1.0f;
    public int damageAttack = 10;
    public float timer = 0.0f;

    

    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        life.OnKilled.AddListener(OnKilled);
        life.OnDamage.AddListener(OnDamageTaken);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (target && CanAttack()) //comprobamos que exista el target y puede atacar
        {
            //reseteamos el ataque
            timer = 0;
            animator.SetTrigger("attack");
            //atacamos al player al estar cerca
            Attack();

        }
    }

    bool CanAttack()
    {
        //comprobamos si el timer esta dentro del rango 
        bool timerInRange = timer >= attackrate;

        //comprobamos si el jugador esta cerca
        bool nearPlayer = Vector3.Distance(transform.position, target.transform.position) < attackdistance; 

        return timerInRange && nearPlayer;
    }

    void Attack()
    {
        Vector3 diff = target.transform.position - transform.position;

        if (Physics.Raycast(transform.position,diff, out RaycastHit hit))
        {
            LifeController life = hit.transform.GetComponent<LifeController>();
            if (life)
            {
                life.ApplyDamage(damageAttack);
            }
        }
    }

    void OnDamageTaken()
    {
        animator.SetTrigger("damaged");
    }
    void OnKilled()
    {
        animator.SetTrigger("killed");
    }
}
