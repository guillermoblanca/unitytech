﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AIMovement : MonoBehaviour
{
    public NavMeshAgent agent;
    public GameObject objective;
    public Animator animator;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (objective == null) return;

        agent.SetDestination(objective.transform.position);
        animator.SetBool("iswalking", !agent.isStopped);
    }
}
