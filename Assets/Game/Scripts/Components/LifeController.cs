﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
/*
quitar vida 
recuperar vida 

vida actual 
vida maxima 

soy invencible?
puedo aumentar mi vida maxima?
*/
public class LifeController : MonoBehaviour
{
    [Header("Attributes")]
    [SerializeField] int currentLife =100;
    [SerializeField] int maxLife =100;

    [Header("Sound effects")] //aparece en el editor el titulo encima del audiosource. Sirve para tenerlo ordenado
    [SerializeField] AudioSource audioSouce;
    [SerializeField] AudioClip clipDamage;
    [SerializeField] AudioClip clipHeal;

    public UnityEvent OnDamage;
    public UnityEvent OnKilled;
    bool isDeath =false; //para comprobar si el jugador esta muerto //¡NUEVO!

    /// <summary>
    /// Realiza daño pudiendo matar al personaje
    /// </summary>
    /// <param name="damage"></param>
    public void ApplyDamage(int damage) //10
    {
        if(isDeath == true) //¡NUEVO!
        {
            return; //se para el codigo aqui si estamos muertos
        }

        currentLife = currentLife - damage;
        Debug.Log("Man hecho daño:( " + damage);
        //muereteeeee
        OnDamage?.Invoke();

        if(audioSouce && clipDamage) //en caso de existir el sonido y el clip lo ejecutamos
        {
            audioSouce.PlayOneShot(clipDamage);
        }

        if(currentLife <= 0)
        {
            currentLife = 0;
            isDeath = true;
            //Muere
            Die();
        }
    }

   /// <summary>
   /// Cura al personaje cuando se llama a la funcion :)
   /// </summary>
   /// <param name="heal"></param>
    public void ApplyHeal(int heal)
    {
        if(isDeath)
        {
            return;
        }
        //curamos al personaje
        currentLife += heal;

        if (audioSouce && clipHeal) //en caso de existir el sonido y el clip lo ejecutamos
        {
            audioSouce.PlayOneShot(clipHeal);
        }

        if (currentLife > maxLife)
        {
            currentLife = maxLife; //no puede superar la vida maxima
        }

    }


    ////////////////////////////////
    //      Controls function
    ////////////////////////////////
    public int GetCurrentLife() { return currentLife; }
    public int GetCurrentMaxLife() { return maxLife; }
    
    
    
    void Die()
    {
        Debug.Log("He morido");
        OnKilled?.Invoke();
    }
}
