﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCharacter : MonoBehaviour
{
    public LifeController life;
    // Start is called before the first frame update
    void Start()
    {
        life.OnKilled.AddListener(OnKilled);
    }

    void OnKilled()
    {
        SceneManager.LoadScene("deathmenu");
    }
}
