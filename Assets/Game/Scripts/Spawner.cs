﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemyPrefab;
    public float spawnRate =2.0f;
    public float timer = 0;
    public float distance = 2.0f;
    public float rot = 120;
    // Start is called before the first frame update
    public int EnemyCount = 5;
    private List<GameObject> enemies = new List<GameObject>();
    void Start()
    {
        for (int i = 0; i < EnemyCount; i++)
        {
            Spawn();
        }
    }

    void Spawn()
    {
        Vector3 spawnPoint = transform.position + new Vector3(Random.Range(-distance, distance), transform.position.y, Random.Range(-distance, distance));
        Quaternion spawnRot = Quaternion.Euler(new Vector3(0, Random.Range(-rot, rot), 0));
        
        GameObject enemy =  Instantiate(enemyPrefab, spawnPoint, spawnRot);

        enemies.Add(enemy);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, distance);
    }
}
