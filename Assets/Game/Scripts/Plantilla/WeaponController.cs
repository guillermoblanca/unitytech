﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEditorInternal;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
 

    [Header("Cadence")]
    public float fireRate = 0.4f;
    public float distance = 100;
    [Header("Bullets")]
    public int maxBulletsInWeapon = 10;
    public int bulletsPerShoot = 1;
    [Header("Damage")]
    public int damage = 10;

    private int bulletsInWeapon = 0;
    private int currentBullets = 0;
    private bool isShooting = false;
    private float timer = 0.0f;
    private Camera cameraRef;

    private const string m_FireInput = "Fire1";
    public void Start()
    {
        bulletsInWeapon = maxBulletsInWeapon;
        cameraRef = Camera.main;
    }


    public void Update()
    {
        timer += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.R))
        {
            Reload();
        }

        if (Input.GetButton(m_FireInput))
        {
            TryToShoot();
        }
        else if (Input.GetButtonUp(m_FireInput))
        {
            StopShoot();
        }
    }

    public void TryToShoot()
    {
        if (isShooting)
        {
            return;
        }

        if (CanShoot())
        {


            StartShoot();
        }
    }

    public bool CanShoot()
    {
        return false;
    }
    private void StartShoot()
    {
        isShooting = true;
        bool hasBullets = ConsumeBullets();

        if (hasBullets)
        {
            Shoot();
        }
        else
        {
            StopShoot();
            Reload();

        }

    }

    private void Shoot()
    {
      
    }
    private void StopShoot()
    {
      
    }
    public void Reload()
    {
      
    }


    public void AddBullets(int bullets)
    {
   
    }

    private bool ConsumeBullets()
    {
        return false;
    }
}