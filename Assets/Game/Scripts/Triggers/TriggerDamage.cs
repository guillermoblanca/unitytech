﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDamage : MonoBehaviour
{
    [SerializeField] int damage = 10;
    private void OnTriggerEnter(Collider other)
    {
        // tienes el lifecontroller?
        LifeController playerLife = other.GetComponent<LifeController>();
        if(playerLife != null) //si!
        {
            //pos te mato
            playerLife.ApplyDamage(damage);
        }
    }
}
