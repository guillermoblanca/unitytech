﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerHealer : MonoBehaviour
{
    [SerializeField] int healValue = 30;
    private void OnTriggerEnter(Collider other)
    {
        LifeController life = other.GetComponent<LifeController>();
        
        if(life)
        {
            life.ApplyHeal(healValue);
        }
    }
}
