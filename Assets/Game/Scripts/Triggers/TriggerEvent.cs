﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour
{
    public UnityEvent OnEnterEvent;
    public UnityEvent OnExitEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(OnEnterEvent != null)
        {
            OnEnterEvent.Invoke();
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if(OnExitEvent != null)
        {

            OnExitEvent.Invoke();
        }
    }
}
