﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupWeapon : MonoBehaviour
{

    public int bullets = 10;


    private void OnTriggerEnter(Collider other)
    {
        FireWeapon weapon = other.GetComponent<FireWeapon>();

        if(weapon)
        {
            weapon.AddBullets(bullets);
            Destroy(gameObject);
        }
    }
}
